FROM python:3.8-alpine

WORKDIR /app

COPY . /app

# RUN pip install -U pip && \
RUN pip install -r requirements.txt --verbose

EXPOSE 80

CMD python3 -u ./src/main.py