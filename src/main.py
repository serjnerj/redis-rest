from aiohttp import web
from socket import * 
import asyncio
import logging
import time
import os
import re

class Application():
    def __init__(self) -> None:
        self.listen_address = os.getenv('LISTEN_ADDR', '0.0.0.0')
        self.listen_port = os.getenv('LISTEN_PORT', 80)
        self.redis_address = os.getenv('REDIS_ADDR', 'redis')
        self.redis_port = os.getenv('REDIS_PORT', 6379)
        # self.redis_auth тут могла быть ваша авторизация

        self.redis_client = RedisClient(self.redis_address, self.redis_port)
        self.webserver = WebServer(self.listen_address, self.listen_port, self.redis_client)

class WebServer():
    def __init__(self, addr, port, redis_client) -> None:
        self.redis_client = redis_client
        app = web.Application()

        app.add_routes([web.get('/', self.handle_keys),
                        web.get('/{tail:.*}', self.handle_get),
                        web.post('/{tail:.*}', self.handle_post),
                        web.delete('/{tail:.*}', self.handle_delete),
                        web.put('/{tail:.*}', self.handle_put),
                        web.patch('/{tail:.*}', self.handle_patch)])

        web.run_app(app=app, host=addr, port=port)

    async def handle_keys(self, req):
        print(f"Recieved query is: {req.rel_url}")

        pattern = req.rel_url.query.get("pattern", "*")
        limit = req.rel_url.query.get("limit", 10)
        list = req.rel_url.query.get("list", 0)

        try: 
            if int(limit) < 0:
                limit = 10
            elif int(limit) > 1000:
                limit = 1000

            if pattern == "":
                pattern = "*"

        except Exception as e:
            print(e)
            return web.json_response(status=400)

        keys = await self.redis_client.getKeys(pattern=pattern, limit=limit, list=list)

        return web.json_response({
            "pattern": pattern,
            "keys": keys,
            "metadata": {
                "total": len(keys),
                "limit": limit,
                "list": list
            }
        })

    async def handle_get(self, req):
        path = req.rel_url.path
        print(f"{time.ctime()} Incoming request path: {path}")

        key = re.sub("/", ":", path[1:])
        data = await self.redis_client.getKey(key)

        if data.get("type") == "none":
            return web.json_response(data=data, status=404)
        else:
            return web.json_response(data=data, status=200)

    async def handle_post(self, req):     
        path = req.rel_url.path
        print(f"{time.ctime()} Incoming request path: {path}")        
        key = re.sub("/", ":", path[1:])

        data = await req.json()
        print(f"Recieved: {data}")

        try:
            if type(data["type"]) == str:
                if data["type"] == "string" or data["type"] == "list" or data["type"] == "set":
                    dataType = data["type"]
                else:
                    return web.json_response(data={
                        "changed": None,
                        "ttl": None,
                        "error": {
                            "message": 'type should be "string", "list" or "set"'
                        }
                    }, status=400)
 
            if type(data["value"]) == str or type(data["value"]) == list:
                value = data["value"]
            else:
                return web.json_response(data={
                    "changed": None,
                    "ttl": None,
                    "error": {
                        "message": 'value should be "string" or "array"'
                    }
                }, status=400)

            if type(int(data.setdefault("ttl", 0))) == int:
                ttl = data["ttl"]
            else:
                return web.json_response(data={
                    "changed": None,
                    "ttl": None,
                    "error": {
                        "message": 'ttl should be integer'
                    }
                }, status=400)

            if dataType == "string" and type(value) == list:
                return web.json_response(data={
                    "changed": None,
                    "ttl": None,
                    "error": {
                        "message": 'value of type "string" should be string'
                    }
                }, status=400)                

        except Exception as e:
            print(e)
            return web.json_response(status=400)

        result = await self.redis_client.setKey(dataType=dataType, key=key, value=value, ttl=ttl)
        return web.json_response(result)


    async def handle_delete(self, req):
        path = req.rel_url.path
        print(f"{time.ctime()} Incoming request path: {path}")        
        key = re.sub("/", ":", path[1:])
        result = await self.redis_client.removeKey(key=key)

        return web.json_response(data=result)

    async def handle_put(self, req):
        return web.json_response(status=418)

    async def handle_patch(self, req):
        return web.json_response(status=418)

        
class RedisClient():
    def __init__(self, addr, port) -> None:
        self.tcp_socket = socket(AF_INET, SOCK_STREAM)
        self.tcp_socket.connect((addr,port))

    # хотя, мб, хорошо бы использовать порой SCAN и пагинацию?..
    async def getKeys(self, pattern, limit, list):
        data = await self.sendData(await self.parseRequest(["KEYS", pattern]))
        return data

    async def getKey(self, key):
        dataType = await self.sendData(await self.parseRequest(["TYPE", key]))
        dataType = dataType.strip()[1:]

        if dataType == "string":
            result = await self.sendData(await self.parseRequest(["GET", key]))
        elif dataType == "list":
            result = await self.sendData(await self.parseRequest(["LRANGE", key, "0", "-1"]))
        elif dataType == "set":
            result = await self.sendData(await self.parseRequest(["SMEMBERS", key]))
        elif dataType == "none":
            print(f"No such key: {key}")
            return {
                "value": None,
                "type": dataType
            }
        else:
            raise ValueError(f"Unspecified data type: {dataType}")
        
        return {
            "value": result,
            "type": dataType
            }

    async def setKey(self, dataType, key, value, ttl):
        changedValues = 0
        isTTLSet = 0

        if dataType == "string" and type(value) == str:
            isKeySet = await self.sendData(await self.parseRequest(["SET", key, value]))
            if isKeySet == "+OK\r\n":
                changedValues = 1

        elif dataType == "list" and type(value) == list:
            changedValues = await self.sendData(await self.parseRequest(["LPUSH", key] + value))
            changedValues = changedValues[1:len(changedValues) - 2]

        elif dataType == "set" and type(value) == list:
            changedValues = await self.sendData(await self.parseRequest(["SADD", key] + value))
            changedValues = changedValues[1:len(changedValues) - 2]
        else:
            raise ValueError(f"Unspecified data type: {dataType}")

        if ttl != 0:
            isTTLSet = await self.sendData(await self.parseRequest(["EXPIRE", key, ttl]))

        return {
            "changed": changedValues,
            "ttl": isTTLSet
        }
    
    async def removeKey(self, key):
        isRemoved = 0

        isRemoved = await self.sendData(await self.parseRequest(["DEL", key]))
        # isRemoved = re.sub("\r\n", "", isRemoved)
        isRemoved = isRemoved[1:len(isRemoved) - 2]


        return {
            "removed": isRemoved
        }


    async def sendData(self, data):
        print(f"{time.ctime()} Request to Redis: {data}")

        data = str.encode(data)
        self.tcp_socket.send(data)
        data = bytes.decode(data)
        data = self.tcp_socket.recv(1024)

        data = await self.parseResponce(data)
        print(f"{time.ctime()} Response from Redis: {data}")
        return data

    async def parseRequest(self, params):
        str = ""

        if type(params) == list and len(params) > 1:
            str = f"*{len(params)}\r\n"
            for param in params:
                str += f"${len(param)}\r\n{param}\r\n"

        return str

    async def parseResponce(self, data):
        data = data.decode("utf-8")
        print(f"{data} | {type(data)}")
        
        if data[0] == "+":
            print(f"it would not parsed: {data}")
        elif data[0] == "*":
            vars = re.split("\r\n" , data)
            print(f"{vars}")

            data = []
            for id, var in enumerate(vars):
                if id!=0 and id%2 == 0:
                    data.append(var)
        elif data[0] == "$":
            vars = re.split("\r\n" , data)
            print(f"{vars}")

            data = vars[1]

        return data


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("asyncio").setLevel(logging.ERROR)
    logging.getLogger("aiohttp").setLevel(logging.ERROR)
    logger = logging.getLogger(__name__)

    loop = asyncio.get_event_loop()

    app = Application()

    loop.run_until_complete(app.start())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(app.stop())
    loop.close()